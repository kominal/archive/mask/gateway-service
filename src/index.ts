import { createServer } from 'net';
import { Socket } from 'net';
import { debug, info } from '@kominal/lib-node-logging';
import { LISTEN_ADDRESS, LISTEN_PORT_4, LISTEN_PORT_6, STACK_NAME } from './helper/environment';
import axios from 'axios';

function forward(clientSocket: Socket, hostAddress: string, hostPort: number) {
	debug(`Forwarding connection to ${hostAddress}:${hostPort}.`);

	const serverSocket = new Socket();
	serverSocket.connect(hostPort, hostAddress, () => {
		clientSocket.pipe(serverSocket);
		serverSocket.pipe(clientSocket);
	});
	clientSocket.on('error', () => serverSocket.end());
	serverSocket.on('error', () => clientSocket.end());
	clientSocket.on('end', () => serverSocket.end());
	serverSocket.on('end', () => clientSocket.end());
}

createServer(async (clientSocket) => {
	debug(`Received connection.`);

	const { data } = await axios.get(`http://${STACK_NAME}_controller:3000/mask/random/4`);
	const { hostAddress, hostPort } = data;

	forward(clientSocket, hostAddress, hostPort);
}).listen(LISTEN_PORT_4, LISTEN_ADDRESS, () => info(`Listening for IPv4 requests to forward on port ${LISTEN_PORT_4}.`));

createServer(async (clientSocket) => {
	debug(`Received connection.`);

	const { data } = await axios.get(`http://${STACK_NAME}_controller:3000/mask/random/6`);
	const { hostAddress, hostPort } = data;

	forward(clientSocket, hostAddress, hostPort);
}).listen(LISTEN_PORT_6, LISTEN_ADDRESS, () => info(`Listening for IPv6 requests to forward on port ${LISTEN_PORT_6}.`));
